# ShortURL

This is a simple URL shortening-app. It was requested by RedPanda. I am using SpringBoot with Thymeleaf technologies simply because micro-services approach is the future! 

## Advantages of Micro-Service 
* If one had to integrate this new system with an existing one, they&#39;d only need to add a link in the old system to point to this service. No other changes.  
*  You spend little time debugging a MicroServiced system because you get to focus on the affected Service alone, without even shutting down the entire system.