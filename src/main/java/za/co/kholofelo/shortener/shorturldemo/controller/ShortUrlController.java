package za.co.kholofelo.shortener.shorturldemo.controller;


import java.util.Optional;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import za.co.kholofelo.shortener.shorturldemo.model.UrlBean;
import za.co.kholofelo.shortener.shorturldemo.service.UrlShoterningService;

@Controller
public class ShortUrlController {


  @Autowired
  private UrlShoterningService urlShoterningService;

  protected String getBaseApplicationURL(HttpServletRequest request) {
    String requestURL = request.getRequestURL().toString();
    requestURL = requestURL.substring(0, requestURL.indexOf(request.getServletPath()));
    return requestURL;
  }

  @GetMapping({"/", "/index"})
  public String getIndexPage(Model model) {
    model.addAttribute("urlBean", new UrlBean());

    return "index";
  }

  @GetMapping("/aboutSystem")
  public String getAboutSystemPage(Model model) {
    return "aboutSystem";
  }


  @PostMapping("/shortenURL")
  public String shortenURL(@ModelAttribute("urlBean") @Valid UrlBean urlBean, BindingResult result,
      HttpServletRequest request, Model model) {

    if (result.hasErrors()) {
      return "index";
    }
    String baseUrl = getBaseApplicationURL(request);
    UrlBean savedUrlBean = urlShoterningService.shortenURL(urlBean, baseUrl);

    model.addAttribute("urlBean", savedUrlBean);

    model.addAttribute("createdShortUrl", savedUrlBean.getShortURL());
    model.addAttribute("createdShortUrlID", savedUrlBean.getId());
    model.addAttribute("oldURL", savedUrlBean.getRawURL());
    model.addAttribute("oldUrlLength", savedUrlBean.getRawURL().length());
    model.addAttribute("newUrlLength", savedUrlBean.getShortURL().length());
    System.out.println(savedUrlBean);
    return "index";
  }

  @GetMapping("/sh/{id}")
  public String launchShortURL(@PathVariable("id") long id) {
    UrlBean urlBean = urlShoterningService.findOne(id);

    return Optional.of(urlBean).map(urlBean1 -> "redirect:" + urlBean1.getRawURL())
        .orElse("redirect:/index?errorNotFound");
  }


}
