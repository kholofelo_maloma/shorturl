package za.co.kholofelo.shortener.shorturldemo.model;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table
public class UrlBean {
  //for lack of a better name, I used "Bean"


  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  private long id;

  @NotEmpty(message = "A URL cannot be empty!")
  @NotNull
  @Pattern(regexp = "^http(s{0,1})://[a-zA-Z0-9_/\\-\\.]+\\.([A-Za-z/]{2,5})[a-zA-Z0-9_/\\&\\?\\=\\-\\.\\~\\%]*", flags = Flag.UNICODE_CASE, message = "Not a valid URL. A valid URL follows the following pattern: http(s)://google.com")
  private String rawURL;
  private String shortURL;


  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public String getRawURL() {
    return rawURL;
  }

  public void setRawURL(String rawURL) {
    this.rawURL = rawURL;
  }

  public String getShortURL() {
    return shortURL;
  }

  public void setShortURL(String shortURL) {
    this.shortURL = shortURL;
  }

  @Override
  public String toString() {
    return "UrlBean{" +
        "id=" + id +
        ", rawURL='" + rawURL + '\'' +
        ", shortURL='" + shortURL + '\'' +
        '}';
  }
}
