package za.co.kholofelo.shortener.shorturldemo.service;

import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import za.co.kholofelo.shortener.shorturldemo.model.UrlBean;
import za.co.kholofelo.shortener.shorturldemo.repository.UrlBeanRepository;

@Service
public class UrlShoterningServiceImpl implements UrlShoterningService {

  @Autowired
  private UrlBeanRepository urlBeanRepository;


  @Override
  public UrlBean shortenURL(UrlBean urlBean, String baseUrl) {

    UrlBean urlBeanFromDatabase = urlBeanRepository.findOneByRawURL(urlBean.getRawURL());

    if (urlBeanFromDatabase != null) {
      urlBeanFromDatabase.setShortURL(createShortURL(urlBeanFromDatabase.getId(), baseUrl));

      return urlBeanRepository.save(urlBeanFromDatabase);
    }
    urlBeanFromDatabase = urlBeanRepository.save(urlBean);
    String shortURL = createShortURL(urlBeanFromDatabase.getId(), baseUrl);
    urlBean.setShortURL(shortURL);
    urlBean.setRawURL(urlBean.getRawURL());

    urlBeanRepository.save(urlBean);
    return urlBeanFromDatabase;
  }



  @Override
  public UrlBean findOne(long id) {
    return urlBeanRepository.findOne(id);
  }

  /**
   * This shortens the URL by reducing it to a database ID which is guaranteed to be unique all the
   * time. Simple solution I must say, yet robust!
   */
  private String createShortURL(long id, String baseUrl) {
    return baseUrl + "/sh/" + id;
  }
}
