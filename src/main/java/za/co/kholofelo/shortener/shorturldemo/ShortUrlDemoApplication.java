package za.co.kholofelo.shortener.shorturldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShortUrlDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShortUrlDemoApplication.class, args);
	}
}
