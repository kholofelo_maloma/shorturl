package za.co.kholofelo.shortener.shorturldemo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import za.co.kholofelo.shortener.shorturldemo.model.UrlBean;

public interface UrlBeanRepository extends JpaRepository<UrlBean, Long> {

  UrlBean findOneByRawURL(String rawUrl);
}
