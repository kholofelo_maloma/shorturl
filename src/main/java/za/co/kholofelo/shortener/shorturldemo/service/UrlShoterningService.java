package za.co.kholofelo.shortener.shorturldemo.service;

import java.util.Optional;
import za.co.kholofelo.shortener.shorturldemo.model.UrlBean;

public interface UrlShoterningService {

  /**
   * Shortens the given URL. Before doing this, for effeciency reasons, see if the record exists on the database already.
   * This avoids having duplicates over long period use, which may slow down the database
   * @param urlBean bean from front end
   * @param baseUrl created bean from database
   * @return
   */
  UrlBean shortenURL(UrlBean urlBean, String baseUrl);

  UrlBean findOne(long id);
}
